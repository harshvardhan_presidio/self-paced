"use strict";

const AWS = require("aws-sdk");
const REGION = process.env.REGION;
AWS.config.update({
    region: REGION
})
const USERS_TABLE = process.env.USERS_TABLE;
const dynamodb = new AWS.DynamoDB.DocumentClient()

var log4js = require("log4js");
var logger = log4js.getLogger("USER");
logger.level = "all"

const validateUserId = async (userId) => {
    try {
        const params = {
            TableName: USERS_TABLE,
            Key: {
                userId: userId
            },
            AttributesToGet: [
                'userId'
            ]
        };
        const {
            Item
        } = await dynamodb.get(params).promise();
        if (Item !== undefined && Item !== null) {
            return true
        } else {
            return false
        }
    } catch (error) {
        logger.fatal(error)
        return false
    }
}

const createNewUserDB = async (user) => {
    try {
        const params = {
            TableName: USERS_TABLE,
            Item: {
                userId: user.userId,
                name: user.userName,
                email: user.email,
                phone: user.phone,
                createdAt: String(user.createdAt)
            },
        };
        await dynamodb.put(params).promise();
        return true
    } catch (error) {
        logger.fatal(error)
        return false
    }
}

const viewAllUserList = async () => {
    const params = {
        TableName: USERS_TABLE,
    };
    const scanResults = [];
    let items
    do {
        items = await dynamodb.scan(params).promise();
        items.Items.forEach((item) => scanResults.push(item));
        params.ExclusiveStartKey = items.LastEvaluatedKey;
    } while (typeof items.LastEvaluatedKey !== "undefined");
    return scanResults;
};

const deleteTheUser = async (userId) => {
    try {
        const Params = {
            TableName: USERS_TABLE,
            Key: {
                userId: userId
            }
        }
        await dynamodb.delete(Params).promise()
        return true
    } catch (error) {
        logger.fatal(error)
        return false
    }
}

const editTheUser = async (user) => {
    try {
        const Params = {
            TableName: USERS_TABLE,
            Key: {
                userId: user.userId
            },
            UpdateExpression: "set  email = :y, phone = :z, #MyVariable = :x",
            ExpressionAttributeNames: {
                "#MyVariable": "name"
            },
            ExpressionAttributeValues: {
                ":x": user.name,
                ":y": user.email,
                ":z": user.phone
            }
        }
        await dynamodb.update(Params).promise()
        return true
    } catch (error) {
        logger.fatal(error)
        return false
    }
}

module.exports = {
    validateUserId: validateUserId,
    createNewUserDB: createNewUserDB,
    viewAllUserList: viewAllUserList,
    deleteTheUser: deleteTheUser,
    editTheUser: editTheUser
}