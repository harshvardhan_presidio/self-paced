"use strict";

const responseValues = require("../constants/responseConst")

const {
  v4
} = require("uuid")

const {
  isNotValidUserId
} = require("../utils/dataSanity");

const {
  createNewUserService,
  viewAllUserListService,
  delteUserService,
  editUserService
} = require("../services/userCrud");

let user = require("../model/userModel");

const {
  validateUserCreation,
  validateUserId,
  validateUserEdit
} = require("../services/validateUser");

const {
  ValidationError
} = require("../errors/errorHandler")

var log4js = require("log4js");
var logger = log4js.getLogger("USER");
logger.level = "all"

const createNewUser = async (event) => {
  try {
    if(event.body!=null){
      try{
        user = JSON.parse(event.body)
      }
      catch (jsonError){
        throw new ValidationError("Invalid-JSON")
      }
    }
    else{
      throw new ValidationError("Empty request body not allowed")
    }
    user.createdAt = new Date();
    let validateResponse = await validateUserCreation(user)
    if (validateResponse != null) {
      return {
        statusCode: 403,
        body: JSON.stringify({
          "message": validateResponse
        })
      }
    }
    user.userId = v4();
    let breakNow = true;
    while (breakNow) {
      let userIdResponse = await isNotValidUserId(user.userId)
      if (userIdResponse) {
        user.userId = v4();
      }
      breakNow = userIdResponse;
    }
    const userCreatedStatus = await createNewUserService(user)
    return userCreatedStatus
  } catch (error) {
    logger.error(error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        "message": responseValues.error.error500,
        "error":error.message
      })
    }
  }
};

const viewAllUser = async (event) => {
  try {
    const userListResult = await viewAllUserListService();
    return userListResult
  } catch (error) {
    logger.error(error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        "message": responseValues.error.error500,
        "error":error.message
      })
    }
  }
}

const deleteUser = async (event) => {
  try {
    if(event.body!=null){
      try{
        user = JSON.parse(event.body)
      }
      catch (jsonError){
        throw new ValidationError("Invalid-JSON")
      }
    }
    else{
      throw new ValidationError("Empty request body not allowed")
    }
    const errorMessage = await validateUserId(user.userId);
    if (errorMessage != null) {
      return errorMessage
    }
    const deleteResponse = await delteUserService(user.userId)
    return deleteResponse
  } catch (error) {
    logger.error(error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        "message": responseValues.error.error500,
        "error":error.message
      })
    }
  }
}

const editUser = async (event) => {
  try {
    if(event.body!=null){
      try{
        user = JSON.parse(event.body)
      }
      catch (jsonError){
        throw new ValidationError("Invalid-JSON")
      }
    }
    else{
      throw new ValidationError("Empty request body not allowed")
    }
    const errorMessage = await validateUserEdit(user)
    if (errorMessage != null) {
      return errorMessage
    }
    const editStatus = await editUserService(user)
    return editStatus
  } catch (error) {
    logger.error(error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        "message": responseValues.error.error500,
        "error":error.message
      })
    }
  }
}

module.exports = {
  viewAllUser: viewAllUser,
  createNewUser: createNewUser,
  deleteUser: deleteUser,
  editUser: editUser
}