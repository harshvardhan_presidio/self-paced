"use strict";

const {
  validateUserId
} = require("../db-interaction/userIntercation")

const isNotValidUserName = async (userName) => {
  if ((userName.length < 5) || (userName.length > 32)) {
    return true
  } else {
    return false
  }
}

const isNotValidUserEmail = async (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!re.test(email)) {
    return true
  } else {
    return false
  }
}

const isNotValidUserPhone = async (phone) => {
  if (phone.length != 10) {
    return true
  } else {
    return false;
  }
}

const isOnlyNumericPhone = (phone) => {
  return /^-?\d+$/.test(phone);
}

const isNotValidUserId = async (userId) => {
  const validStatus = await validateUserId(userId)
  return validStatus
}

module.exports = {
  isNotValidUserName: isNotValidUserName,
  isNotValidUserEmail: isNotValidUserEmail,
  isNotValidUserPhone: isNotValidUserPhone,
  isNotValidUserId: isNotValidUserId,
  isOnlyNumericPhone: isOnlyNumericPhone
}