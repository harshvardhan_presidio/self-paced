"use strict";

let valueHandler = {
    success: {
        userCreated: "User created successfully",
        userDeleted: "User deleted successfully",
        userEdited: "User edited succesfully",
        allUser: "All user fetched"
    },
    error: {
        error500: "Something is not right! Please try again later"
    },
    failure: {
        invalidEmail: "Invalid EMAIL provided",
        invalidUserId: "Invalid User Id provided",
        invalidPhone: "Invalid phone provided",
        invalidName: "Invalid name provided",
        noUser: "No user to display",
        issueDelete: "Issue occured while deleting the user",
        issueCreate: "Issue occured while creating the user",
        issueEdit: "Issue occured while editing the user"
    },
    empty: {
        emptyEmail: "No email provided",
        emptyName: "No name provided",
        emptyPhone: "No phone provided",
        emptyId: "No user id provided"
    }
}

module.exports = Object.freeze(valueHandler);