const user = {
  userId: String,
  email: String,
  name: String,
  phone: String,
  createdAt: String
};
module.exports = user;