"use strict";

const {
    isNotValidUserName,
    isNotValidUserEmail,
    isOnlyNumericPhone,
    isNotValidUserPhone,
    isNotValidUserId
} = require("../utils/dataSanity");

const responseValues = require("../constants/responseConst")

const validateUserCreation = async (user) => {
    let validateResponse = false;
    if ("email" in user) {
        validateResponse = await isNotValidUserEmail(user.email)
        if (validateResponse) {
            return responseValues.failure.invalidEmail
        }
    } else {
        return responseValues.empty.emptyEmail
    }

    if ("name" in user) {
        validateResponse = await isNotValidUserName(user.name)
        if (validateResponse) {
            return responseValues.failure.invalidName
        }
    } else {
        return responseValues.empty.emptyName
    }

    if ("phone" in user) {
        if (isOnlyNumericPhone(user.phone)) {
            validateResponse = await isNotValidUserPhone(user.phone)
            if (validateResponse) {
                return responseValues.failure.invalidPhone
            }
        } else {
            return responseValues.failure.invalidPhone
        }
    } else {
        return responseValues.empty.emptyPhone
    }

    return null
}

const validateUserId = async (userId) => {
    const isValid = await isNotValidUserId(userId)
    if (isValid == false) {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.failure.invalidUserId
            })
        }
    }
    return null
}

const validateUserEdit = async (user) => {
    let validateResponse = false;
    if ("email" in user) {
        validateResponse = await isNotValidUserEmail(user.email)
        if (validateResponse) {
            return {
                statusCode: 403,
                body: JSON.stringify({
                    "message": responseValues.failure.invalidEmail
                })
            }
        }
    } else {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.empty.emptyEmail
            })
        }
    }

    if ("name" in user) {
        validateResponse = await isNotValidUserName(user.name)
        if (validateResponse) {
            return {
                statusCode: 403,
                body: JSON.stringify({
                    "message": responseValues.failure.invalidName
                })
            }
        }
    } else {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.empty.emptyName
            })
        }
    }

    if ("phone" in user) {
        if (isOnlyNumericPhone(user.phone)) {
            validateResponse = await isNotValidUserPhone(user.phone)
            if (validateResponse) {
                return {
                    statusCode: 403,
                    body: JSON.stringify({
                        "message": responseValues.failure.invalidPhone
                    })
                }
            }
        } else {
            return {
                statusCode: 403,
                body: JSON.stringify({
                    "message": responseValues.failure.invalidPhone
                })
            }
        }
    } else {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.empty.emptyPhone
            })
        }
    }

    if ("userId" in user) {
        const isValid = await isNotValidUserId(user.userId)
        if (isValid == false) {
            return {
                statusCode: 403,
                body: JSON.stringify({
                    "message": responseValues.failure.invalidUserId
                })
            }
        }
    } else {
        return responseValues.empty.emptyId
    }

    return null
}

module.exports = {
    validateUserCreation: validateUserCreation,
    validateUserId: validateUserId,
    validateUserEdit: validateUserEdit
}