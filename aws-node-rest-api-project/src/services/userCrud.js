"use strict";

const responseValues = require("../constants/responseConst")
const {
    createNewUserDB,
    viewAllUserList,
    deleteTheUser,
    editTheUser
} = require("../db-interaction/userIntercation")


const createNewUserService = async (user) => {
    let newUserCreated = await createNewUserDB(user)
    if (newUserCreated) {
        return {
            statusCode: 200,
            body: JSON.stringify({
                "message": responseValues.success.userCreated,
                "userInfo": user
            })
        }
    } else {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.failure.issueCreate
            })
        }
    }
}

const viewAllUserListService = async () => {
    const userList = await viewAllUserList()
    if (userList.length > 0) {
        return {
            statusCode: 200,
            body: JSON.stringify({
                "message": responseValues.success.allUser,
                "users": userList
            })
        }
    } else {
        const returnValue = {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.failure.noUser
            })
        }
        return returnValue
    }
}

const delteUserService = async (userId) => {
    const isScuccess = await deleteTheUser(userId)
    if (isScuccess) {
        return {
            statusCode: 200,
            body: JSON.stringify({
                "message": responseValues.success.userDeleted
            })
        }
    } else {
        const returnValue = {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.failure.issueDelete
            })
        }
        return returnValue
    }
}

const editUserService = async (user) => {
    const isScuccess = await editTheUser(user)
    if (isScuccess) {
        return {
            statusCode: 200,
            body: JSON.stringify({
                "message": responseValues.success.userEdited
            })
        }
    } else {
        return {
            statusCode: 403,
            body: JSON.stringify({
                "message": responseValues.failure.issueEdit
            })
        }
    }
}

module.exports = {
    createNewUserService: createNewUserService,
    viewAllUserListService: viewAllUserListService,
    delteUserService: delteUserService,
    editUserService: editUserService
}